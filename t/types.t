use warnings;
use strict;
use utf8;
use Test::More;

use Mouse::Util::TypeConstraints qw(find_type_constraint);

BEGIN {
    use_ok 'MyApp::Types';
};

subtest "UInt test" => sub {
    my $type = find_type_constraint("UInt");
    is $type->check(0),  1;
    is $type->check(-1), '';
};

done_testing;

