use strict;
use warnings;

use Test::More;
use Test::Fatal qw/dies_ok/;

BEGIN {
    use_ok 'MyApp::Foo';
};

subtest 'sample' => sub {
    my $foo = MyApp::Foo->new;

    is $foo->bar( num1 => 1, num2 => 2 ), 3;

    dies_ok {
        $foo->bar(
            num1 => -1, # must num1 >= 0
            num2 => 2
        ), 3;
    };

    is $foo->bar( num1 => 1, num2 => -1 ), 0;
};

done_testing;
