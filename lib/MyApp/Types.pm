package MyApp::Types;
use strict;
use warnings;

use Mouse::Util::TypeConstraints;

subtype 'UInt'
    => as 'Int'
    => where { $_ >= 0 };

1;
