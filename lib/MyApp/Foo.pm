package MyApp::Foo;
use strict;
use warnings;
use 5.018;

use MyApp::Types;
use Data::Validator;

use Mouse;
no Mouse;

sub bar {
    my $self = shift;
    state $validator = Data::Validator->new(
        num1  => 'UInt',
        num2  => 'Int',
    );
    my $args = $validator->validate(@_);

    return $args->{num1} + $args->{num2}
}

1;

